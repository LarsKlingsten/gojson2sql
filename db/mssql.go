package db

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

var f = fmt.Sprintf
var p = fmt.Println

/*SQLDebug adds debugging for each details to each database request
 1 log errors
 2 log messages
 4 log rows affected
 8 trace sql statements
16 log statement parameters
32 log transaction begin/end
*/
var SQLDebug = 0

// MsSQL structure -> for MS SQL SERVERS
type MsSQL struct {
	*sql.DB
	server models.Server
}

//GetConfig return the configuration
func (msSQL *MsSQL) GetConfig() models.Server {
	return msSQL.server
}

// NewMsSQL contructor
func NewMsSQL(server models.Server) (*MsSQL, error) {
	//sn.Log2(sn.LogInitMsg, f("init /db/db.go db=%s server=%s count=%d sqlDebug=%d", config.Name, config.ServerIP, count, SQLDebug))

	addDbDebug := f(";log=%d", SQLDebug) // see https://github.com/denisenkom/go-mssqldb under "log"
	db, err := sql.Open("mssql", server.ConnectionStringMsSQL()+addDbDebug)

	if err != nil {
		return nil, errors.New(f("failed to login to server=%s errMsg=%s ", server, err.Error()))
	}
	err = db.Ping()
	if err != nil {

		sn.Log2(sn.LogError, "@NewMsSQL connStr="+server.ConnectionStringMsSQL()+addDbDebug)
		sn.Log2(sn.LogError, "@NewMsSQL err="+err.Error())

		return nil, errors.New(f("Login Failed: svr='%s' db='%s' userID='%s' pass='%s' -> errMsg=%s ",
			server.ServerIP, server.Name, server.UserID, server.Password, err.Error()))
	}
	return &MsSQL{db, server}, nil
}

// DatabaseName is the name of each connection
func (msSQL *MsSQL) DatabaseName() string {
	return msSQL.server.Name
}

// Query is a generic function that takes statements, and values
// returns sql rows for further processing
func (msSQL *MsSQL) Query(statement string, values []interface{}) (*sql.Rows, error) {

	stmt, err := msSQL.Prepare(statement)
	if err != nil {
		return nil, err
	}

	rows, err := stmt.Query(values...)
	if err != nil {
		return nil, err
	}

	return rows, err
}

// ExecStmt is a generic function that inserts, update or deletes, returns ID, Rows Effected, and Error
// para "statement" could be `update accountEntry 	set Debit = ?, credit = ? where id = ?"
// para "values""   could be values  such 123, 343, 23 (i.e. debit, credit, id)
// return -1 (and no errors) if the no new id was created (as in an update)
func (msSQL *MsSQL) ExecStmt(statement string, values ...interface{}) (int64, int64, error) {
	stmt, err := msSQL.Prepare(statement)
	if err != nil {
		sn.Log2(sn.LogError, f("@DB @ExecuteStatement @Prep. %s", err))
		return -1, -1, err
	}

	res, err := stmt.Exec(values...)
	if err != nil {
		sn.Log2(sn.LogError, f("@DB @ExecuteStatement @Exec. %s", err))
		return -1, -1, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		id = -1
	}

	rows, err := res.RowsAffected()
	if err != nil {
		rows = -1
	}

	return id, rows, nil
}

// ExecStmt2 not implemented do not use for MsSQL
func (msSQL *MsSQL) ExecStmt2(statement string) (int64, int64, error) {
	msg := "MsSQL ExecStmt2 is not implemented"
	log.Fatal(msg)
	return -1, -1, errors.New("msg")
}

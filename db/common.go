package db

import (
	"encoding/json"
	"errors"

	"reflect"

	"bitbucket.com/larsklingsten/gojson2sql/models"
)

// QueryCommon is common implementation for MsSQL, MySQL, and SQLite
// it converts rows into columnNames, row data
// You must call defer rows.close() from whereever you call this
func QueryCommon(iDatabase models.IDatabase, statement string, values []interface{}) ([]byte, int, error) {
	rowsArray := []map[string]interface{}{}
	rowsCount := 0

	rows, err := iDatabase.Query(statement, values) // run either on SQLite, or MsSQL implementations
	if err != nil {
		return nil, -1, err
	}
	defer rows.Close()

	columns, _ := rows.Columns() // can't go wrong!
	var parsedValues = make([]interface{}, len(columns))

	for i := range parsedValues { // create references to parsedValues
		var ii interface{}
		parsedValues[i] = &ii
	}

	for rows.Next() { //loop
		rowsCount++
		myMap := make(map[string]interface{}) // init map on each loop
		err := rows.Scan(parsedValues...)     // parse values
		if err != nil {
			return nil, -1, errors.New(f("@select @rows.Scan() err=%v", err))
		}

		for i := range columns { // parseValues to map with columnname as key && value as value
			var rawValue = *(parsedValues[i].(*interface{}))
			myMap[columns[i]] = ValueConvertion(rawValue) // take care of mySQL
		}
		rowsArray = append(rowsArray, myMap)
	}

	jsonBytes, err := json.Marshal(rowsArray)
	if err != nil {
		return nil, rowsCount, err
	}

	return jsonBytes, rowsCount, nil

}

// ValueConvertion changes variances in field type to the same type.
// MySQL && SQLite returns strings as []Uint (which is an alias for a []byte)
// however we want a string.
func ValueConvertion(rawValue interface{}) interface{} {

	switch reflect.TypeOf(rawValue).Kind() {

	case reflect.Slice: // mySQL return []Uint we want a string
		return string(rawValue.([]byte))

	default:
		return rawValue
	}
}

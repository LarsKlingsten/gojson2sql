package db

import (
	"database/sql"
	"os"

	"bitbucket.com/larsklingsten/gojson2sql/models"
)

// SQLiteDB struct
type SQLiteDB struct {
	server models.Server
}

// NewSQLite contructor
func NewSQLite(config models.Server) (*SQLiteDB, error) {
	newDB := &SQLiteDB{server: config}
	return newDB, nil
}

// DatabaseName is the name of each file/connection
func (sqlite *SQLiteDB) DatabaseName() string {
	return sqlite.server.Name
}

func (sqlite *SQLiteDB) openDB() (*sql.DB, error) {
	db, err := sql.Open("sqlite3", "./"+sqlite.server.Name+".db")
	if err != nil {
		return nil, err
	}
	return db, nil
}

// DeleteDBFile return nil or error if database file was deleted
func (sqlite *SQLiteDB) DeleteDBFile() error {
	err := os.Remove("./" + sqlite.server.Name + ".db")
	if err != nil {
		return err
	}
	return nil
}

// Query return raw rows for further handling (calling function close DB if Sqlite, and close rows)
func (sqlite *SQLiteDB) Query(statement string, values []interface{}) (*sql.Rows, error) {

	db, err := sqlite.openDB()
	if err != nil {
		return nil, err
	}
	defer db.Close() // this may cause early termination prior rows being handled

	stmt, err := db.Prepare(statement)
	if err != nil {
		return nil, err
	}

	rows, err := stmt.Query(values...)
	if err != nil {
		return nil, err
	}
	return rows, err
}

// ExecStmt is a generic function that inserts, update or deletes, returns ID, Rows Effected, and Error
// para "statement" could be `update accountEntry 	set Debit = ?, credit = ? where id = ?"
// para "values""   could be values  such 123, 343, 23 (i.e. debit, credit, id)
// return -1 (and no errors) if the no new id was created (as in an update)
func (sqlite *SQLiteDB) ExecStmt(statement string, values ...interface{}) (int64, int64, error) {

	db, err := sqlite.openDB()
	if err != nil {
		return -1, -1, err
	}
	defer db.Close()

	tx, err := db.Begin()
	if err != nil {
		return 0, 0, err
	}

	stmt, err := tx.Prepare(statement)
	if err != nil {

		return -1, -1, err
	}
	defer stmt.Close()

	_, err = stmt.Exec(values...)
	if err != nil {
		tx.Rollback()
		return 0, 0, err
	}

	err = tx.Commit()
	if err != nil {
		return 0, 0, err
	}
	return 0, 0, nil
}

// ExecStmt2 is a generic function that inserts, update or deletes, returns ID, Rows Effected, and Error
// its does not use statement in combination with values - so be carefull with SQL Injections
func (sqlite *SQLiteDB) ExecStmt2(statement string) (int64, int64, error) {

	db, err := sqlite.openDB()
	if err != nil {
		return -1, -1, err
	}
	defer db.Close()

	result, err := db.Exec(statement)
	if err != nil {
		return 0, 0, err
	}

	lastID, err := result.LastInsertId()
	if err != nil {
		return -1, -1, err
	}

	rowsEffected, err := result.RowsAffected()
	if err != nil {
		return lastID, -1, err
	}

	return lastID, rowsEffected, nil
}

// GetConfig returns configuration
func (sqlite *SQLiteDB) GetConfig() models.Server {
	return sqlite.server
}

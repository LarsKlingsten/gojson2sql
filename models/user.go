package models

// User access level
type User struct {
	Name     string
	Password string
	Select   bool
	Update   bool
	Insert   bool
	Delete   bool
	Drop     bool
	Create   bool
}

func (user *User) String() string {
	return f("User:%s select:%t update=%t insert=%t delete=%t",
		user.Name, user.Select, user.Update, user.Insert, user.Delete)
}

package models

import (
	"fmt"

	"bitbucket.com/larsklingsten/gojson2sql/enums"
)

// Server is the details required to connect to a MS SQL Server
type Server struct {
	Name       string
	IsEnabled  bool
	ServerIP   string
	Port       int
	UserID     string
	Password   string
	ServerType enums.ServerType
	Users      []User
}

// ConnectionStringMsSQL get the MS SQL Server connection string
func (server *Server) ConnectionStringMsSQL() string {
	return fmt.Sprintf("server=%s;database=%s;user id=%s;password=%s;port=%d",
		server.ServerIP,
		server.Name,
		server.UserID,
		server.Password,
		server.Port)
}

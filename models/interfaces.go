package models

import (
	"database/sql"
)

// IDatabase interface that must be implemented to in *db files as otherwise is not accessible
type IDatabase interface {

	// generic queries or statements
	Query(statement string, values []interface{}) (*sql.Rows, error)

	// ExecStmt is a generic function that inserts, update or deletes.
	// para "statement" could be `update accountEntry 	set Debit = ?, credit = ? where id = ?"
	// para "values""   could be values  such 123, 343, 23 (i.e. debit, credit, id)
	// returns lastId, RowsEffected, error
	ExecStmt(statement string, values ...interface{}) (int64, int64, error)

	// ExecStmt2 is a generic function that inserts, update or deletes, returns ID, Rows Effected, and Error
	// its does not use statement in combination with values - so be carefull with SQL Injections
	ExecStmt2(statement string) (int64, int64, error) // lastId, RowsEffected, error

	DatabaseName() string

	GetConfig() Server
}

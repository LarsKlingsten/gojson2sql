package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"bitbucket.com/larsklingsten/gojson2sql/enums"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// Config is the top configuration entry
type Config struct {
	Servers []Server
}

const configFile = "gojson2sql.json"

var f = fmt.Sprintf

//LoadConfig loads file "config.json" and populates
func LoadConfig() ([]Server, error) {
	file, err := sn.FileRead(configFile)
	if err != nil {
		return nil, err
	}

	var config Config
	err = json.Unmarshal(file, &config)
	if err != nil {
		return nil, err
	}
	sn.Log2(sn.LogInitMsg, "init /models/config.go")
	return config.Servers, nil
}

// LoadDatabasesFromConfig load config.json
func LoadDatabasesFromConfig() ([]Server, error) {
	var err error
	databases, err := LoadConfig()
	if err != nil {
		return nil, errors.New(f("failed to load config file. ErrMsg=%s", err.Error()))
	}

	return databases, nil
}

// CreateSampleConfig creates a sample configuration if none exists
func CreateSampleConfig() {

	if sn.FileExist(configFile) {
		sn.LogFatal(f("The file '%s' already exist. We wont overwrite it. Program Ends", configFile))
		os.Exit(1)
	}

	users := []User{
		User{Name: "admin", Password: "secret", Select: true, Insert: true, Update: true, Delete: true},
		User{Name: "read", Password: "secret", Select: true, Insert: false, Update: false, Delete: false},
	}

	servers := []Server{
		Server{
			Name:       "accountDB",
			IsEnabled:  true,
			Users:      users[1:],
			ServerIP:   "sql16vm\\sqlexpress",
			Port:       1433,
			UserID:     "sa",
			Password:   "usual",
			ServerType: enums.MsSQL},
		Server{
			Name:       "note",
			IsEnabled:  true,
			Users:      users[1:],
			ServerIP:   "sql16vm\\sqlexpress",
			Port:       1433,
			UserID:     "sa",
			Password:   "short",
			ServerType: enums.MsSQL},
		Server{
			Name:       "emaildb",
			IsEnabled:  true,
			Users:      users[:1],
			ServerIP:   "xpserver.overseas.com.sg\\sqlexpress",
			Port:       1433,
			UserID:     "sa",
			Password:   "short",
			ServerType: enums.MsSQL},
		Server{
			Name:       "Sqlite-test",
			IsEnabled:  true,
			Users:      users[1:],
			ServerIP:   "",
			Port:       0,
			UserID:     "",
			Password:   "",
			ServerType: enums.SQLite},
	}

	config := Config{Servers: servers}
	j, _ := json.Marshal(config)

	err := sn.FileWrite(configFile, j)
	if err != nil {
		sn.LogFatal(f("Could not write file '%s' %s", configFile, err.Error()))
	}
	sn.Log(f("Successfully added '%s'", configFile))
}

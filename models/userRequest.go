package models

import (
	"encoding/json"

	sn "bitbucket.com/larsklingsten/gosnippets"
)

// UserRequest is the request sent by the users
type UserRequest struct {
	User       string
	Password   string
	Database   string                 // database name
	Table      string                 // table name
	Query      string                 // used for select queries (use ? for parameters)
	Paras      []interface{}          // parameters for select queries, subsitutes ?
	Data       map[string]interface{} // for update and insert statements
	Primarykey string                 // for update, and delete statements (if not set, we use "ID")

}

// JSONAsBytes return the stuct as Json
func (ur *UserRequest) JSONAsBytes() []byte {
	j, _ := json.Marshal(ur)
	return j
}

// AvoidSQLInjection attemps to remove possible SQL injections chars such a [- '
// Table allows only a-Z0-9
func (ur *UserRequest) AvoidSQLInjection() {
	ur.Table = sn.StringRemoveNonAlphaNum(ur.Table)
	ur.Query = sn.StringRenoveNonSQLchars(ur.Query)

}

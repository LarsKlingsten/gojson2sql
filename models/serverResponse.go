package models

import "encoding/json"

// ServerResponse is the response to post and get requests that gives data, isSuccess, and a UserMessage
type ServerResponse struct {
	IsSuccess bool
	Message   string
	Data      json.RawMessage
	Error     string
	Rows      int
	ID        int64
}

// JSON return the stuct as Json
func (sr *ServerResponse) JSON() string {
	return string(sr.JSONAsBytes())
}

var noData = []byte("{}")

// JSONAsBytes return the stuct as Json
func (sr *ServerResponse) JSONAsBytes() []byte {
	if sr.Data == nil { // fix -> as marshal below otherwise fails will nil value
		sr.Data = noData
	}
	j, _ := json.Marshal(sr)
	return j
}

package service

import (
	"bytes"
	"errors"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// SQLDelete delete a row in a database
func (svc *Service) SQLDelete(userReq models.UserRequest) (int64, int64, error) {

	err := svc.verifyDeleteUserRequest(userReq)
	if err != nil {
		return 0, -1, err
	}

	db, err := svc.FindDatabase(userReq.Database)
	if err != nil {
		return 0, -1, err
	}

	sqlStatement, values, err := svc.extractValuesForDelete(userReq)
	if err != nil {
		return 0, -1, err
	}
	id, rows, err := db.ExecStmt(sqlStatement, values...)

	if err != nil {
		sn.Log2(sn.LogError, f("@svc.SQLDelete err   =%v", err.Error()))
		sn.Log2(sn.LogError, f("@svc.SQLDelete stmt  =%v", sqlStatement))
		sn.Log2(sn.LogError, f("@svc.SQLDelete values=%v", values))
		return id, rows, err
	}
	return id, rows, err
}

func (svc *Service) verifyDeleteUserRequest(userReq models.UserRequest) error {
	const help = " Run the server with option --help"
	if userReq.Table == "" {
		return errors.New("Can't update record without a valid table name." + help)
	}
	return nil
}

// extractValuesForDelete
// UserRequest.Table like "notes"
// UserRequest.Data like   {   "id": 242      }
func (svc *Service) extractValuesForDelete(userReq models.UserRequest) (string, []interface{}, error) {
	var sqlStmt bytes.Buffer
	var values []interface{}

	// get the first key/value pair
	keys := []string{}
	for k := range userReq.Data {
		keys = append(keys, k)
	}
	primaryKey := sn.StringRemoveNonAlphaNum(keys[0])
	value := userReq.Data[primaryKey]

	sqlStmt.WriteString(f("DELETE FROM %s where %s = ?  ", userReq.Table, primaryKey))
	values = append(values, value)

	//	sn.Log(string(sqlStmt.Bytes()))

	return string(sqlStmt.Bytes()), values, nil
}

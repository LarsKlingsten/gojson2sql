package service

import (
	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// SQLCreateTable creates a table in database
func (svc *Service) SQLCreateTable(userRequest models.UserRequest) (int64, int64, error) {

	db, err := svc.FindDatabase(userRequest.Database)
	if err != nil {
		return -1, -1, err
	}

	stmt := f("CREATE %s ", userRequest.Query)
	// sn.Log(f("Create Table=" + stmt))
	return db.ExecStmt2(stmt)
}

// SQLDropTable drops a table in database
func (svc *Service) SQLDropTable(userRequest models.UserRequest) (int64, int64, error) {

	db, err := svc.FindDatabase(userRequest.Database)
	if err != nil {
		return -1, -1, err
	}

	stmt := f("DROP %s ", userRequest.Query)
	sn.Log(f("DROP Table=" + stmt))
	return db.ExecStmt2(stmt)
}

package service

import (
	"bytes"
	"errors"
	"strings"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// SQLInsert inserts a row in a database - return values are ID, rows, err
func (svc *Service) SQLInsert(userReq models.UserRequest) (int64, int64, error) {

	err := svc.verifyInsertUserRequest(userReq)
	if err != nil {
		return 0, -1, err
	}

	db, err := svc.FindDatabase(userReq.Database)
	if err != nil {
		return -1, -1, err
	}

	sqlStatement, values, err := svc.extractValuesForInsert(userReq)
	if err != nil {
		return -1, -1, err
	}

	// execute on database statement
	id, rows, err := db.ExecStmt(sqlStatement, values...)

	if err != nil {
		return id, rows, err
	}
	return id, rows, err

}

func (svc *Service) verifyInsertUserRequest(userReq models.UserRequest) error {
	const help = " Run the server with option --help"

	if userReq.Table == "" {
		return errors.New("Can't update record without a valid table name." + help)
	}

	return nil
}

// InsertExtractSQLAndValues executes a insert statement
// and INSERT INTO Client ( CompanyID , Name) VALUES (?,?)
func (svc *Service) extractValuesForInsert(userReq models.UserRequest) (string, []interface{}, error) {
	var sqlStmt bytes.Buffer
	var values []interface{}
	var tableFields []string
	var questionMarks []string

	// if the user has not set the 'primary key' we try with 'id'
	var primaryKey = "id"
	if userReq.Primarykey != "" {
		primaryKey = userReq.Primarykey
	}

	//var timeValue time.Time

	for k, v := range userReq.Data {

		if k == primaryKey { // as this should be set with automatically
			continue
		}

		tableFields = append(tableFields, k)
		questionMarks = append(questionMarks, "?")

		// test if v (value) is a RCF3339 Timestamp (that is what Go is using to marshal)
		date, err := sn.ConvertRFC3339StringToTime(v)
		if err != nil {
			values = append(values, v) // is not a time rcf339 stamp
		} else {
			values = append(values, date) // is a time rcf339 stamp
		}

	}

	fieldsStr := strings.Join(tableFields, ", ")
	questionMarksStr := strings.Join(questionMarks, ", ")
	sqlStmt.WriteString(f("insert into %s (%s) values (%s) ", userReq.Table, fieldsStr, questionMarksStr))

	return string(sqlStmt.Bytes()), values, nil
}

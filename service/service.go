package service

import (
	"errors"
	"fmt"
	"os"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// Service is the intermedia between controllers and database
type Service struct {
	db []models.IDatabase
}

var f = fmt.Sprintf
var p = fmt.Println

// sqlServer is the list of servers that A user has access to
var databases []models.IDatabase

// NewService is constructor. Inject database dependency in constructor
func NewService(newdb []models.IDatabase) (*Service, error) {
	sn.Log2(sn.LogInitMsg, "init /service/service.go")

	databases = newdb
	if newdb == nil {
		sn.LogFatal("Can not connect to Server or Database. Run again with para --help or try --newconfig")
		os.Exit(1)
	}

	return &Service{db: newdb}, nil
}

// FindDatabase returns a db instance
func (svc *Service) FindDatabase(databaseName string) (models.IDatabase, error) {
	for _, db := range svc.db {
		//	p(f("database = %s == %s  is %v", db.DatabaseName(), databaseName, db.DatabaseName() == databaseName))
		if db.DatabaseName() == databaseName {
			return db, nil
		}
	}
	return nil, errors.New("Could not find requested database ")
}

// PrintDatabase lets the databse
func (svc *Service) PrintDatabase() {
	for _, myDb := range svc.db {
		for _, u := range myDb.GetConfig().Users {
			sn.Log2(sn.LogDebug, f("Per Database db=%s User=%s", myDb.DatabaseName(), u.String()))
		}
	}
}

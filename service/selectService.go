package service

import (
	"bitbucket.com/larsklingsten/gojson2sql/db"
	"bitbucket.com/larsklingsten/gojson2sql/models"
)

// SQLSelect safely queries a database
func (svc *Service) SQLSelect(userRequest models.UserRequest) ([]byte, int, error) {

	database, err := svc.FindDatabase(userRequest.Database)
	if err != nil {
		return nil, -1, err
	}

	stmt := f("select %s ", userRequest.Query)
	myJSON, rowsCount, err := db.QueryCommon(database, stmt, userRequest.Paras)
	if err != nil {
		return nil, -1, err
	}
	//	sn.Log(f(" JSON=%v  ", string(myJSON)))

	return myJSON, rowsCount, nil
}

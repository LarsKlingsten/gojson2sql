package service

import (
	"bytes"
	"errors"
	"strings"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// SQLUpdate update a row in a database
func (svc *Service) SQLUpdate(userReq models.UserRequest) (int64, error) {

	err := svc.verifyUpdateUserRequest(userReq)
	if err != nil {
		return 0, err
	}

	db, err := svc.FindDatabase(userReq.Database)
	if err != nil {
		return 0, err
	}

	sqlStatement, values, err := svc.extractValuesForUpdate(userReq)
	if err != nil {
		return 0, err
	}

	_, rows, err := db.ExecStmt(sqlStatement, values...)
	if err != nil {
		sn.Log2(sn.LogError, f("@DB.UpdateRow @Error: %v", sqlStatement))
		sn.Log2(sn.LogError, f("@DB.UpdateRow @Error: %v", values))
		return 0, err
	}
	return rows, err

}

func (svc *Service) verifyUpdateUserRequest(userReq models.UserRequest) error {
	const help = " Run the server with option --help"
	if userReq.Data[userReq.Primarykey] == nil && userReq.Data["id"] == nil {
		return errors.New("Can't update record without a valid primary key and/or 'id':[int] set in data." + help)
	}

	if userReq.Table == "" {
		return errors.New("Can't update record without a valid table name." + help)
	}

	return nil
}

// extractValuesForUpdate is a generic function that extract sql statement, and values from a map
// used with db.ExecuteStatement(sqlStatement, values...)
// return a string 'UPDATE Client SET  CompanyID = ?,CustomID = ? WHERE ID = ?'
// and INSERT INTO Client ( CompanyID ) VALUES (?,?)
// Do NOT call this outside (db *DB)
func (svc *Service) extractValuesForUpdate(userReq models.UserRequest) (string, []interface{}, error) {
	var sqlStmt bytes.Buffer
	var values []interface{}
	var tableFields []string

	// if the user has not set the 'primary key' we try with 'id'
	var primaryKey = "id"
	if userReq.Primarykey != "" {
		primaryKey = userReq.Primarykey
	}

	sqlStmt.WriteString(f("UPDATE %s SET ", userReq.Table)) // start of SQL statement

	for k, v := range userReq.Data {

		// do not add the primary key as data to be updated (as this will fail the server with error)
		if k == primaryKey {
			continue
		}
		tableFields = append(tableFields, k)
		values = append(values, v)
	}
	fieldsStr := strings.Join(tableFields, "=?, ") + "=?"          // the last tableField is not getteting a '=?', so we add it
	sqlStmt.WriteString(f("%s WHERE %s=?", fieldsStr, primaryKey)) // like this -> " CompanyID = ?,CustomID = ? WHERE ID = ? "
	values = append(values, userReq.Data[primaryKey])

	return string(sqlStmt.Bytes()), values, nil
}

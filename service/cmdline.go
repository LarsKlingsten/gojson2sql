package service

import (
	"flag"
	"fmt"
	"os"
	"runtime"

	"bitbucket.com/larsklingsten/gojson2sql/db"
	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// Version number for the server
const Version = "v0.04.2"

// AppName is the application name
const AppName = "'Go JSON-2-SQL'"

// Copyright is the displays the year and copyright holder
const Copyright = "©2018 Overseas Chartering Pte Ltd, Singapore"

// CommandLineOptions set/loads the console run options
func CommandLineOptions() {
	help := flag.Bool("help", false, "displays help")
	samples := flag.Bool("samples", false, "displays samples")
	versionFlag := flag.Bool("version", false, "Displays version number")
	logString := flag.String("logstring", "", "displays message on each log entry")
	newconfig := flag.Bool("newconfig", false, "Creates new config file")
	logLevel := flag.Uint("loglevel", 91, "displays user request")
	logLevelSQL := flag.Int("loglevelsql", 0, "debug level for sql server")

	flag.Parse()

	sn.UserSetLogLevel = sn.LogLevel(*logLevel) // converts uint to LogLevel type
	db.SQLDebug = *logLevelSQL
	sn.LogString = *logString

	if *help {
		displayHelp()
		os.Exit(0)
	}
	if *samples {
		displaySamples()
		os.Exit(0)
	}

	if *versionFlag {
		displayVersion()
		os.Exit(0)
	}
	if *newconfig {
		models.CreateSampleConfig()
		os.Exit(0)
	}
	sn.Log2(sn.LogInitMsg, f("init %s Version=%s %s <ctrl-c> to stop", AppName, Version, Copyright))
	sn.Log2(sn.LogInitMsg, f("loglevel=%v, loglevelSQL=%v Compiler=%s", sn.UserSetLogLevel, *logLevelSQL, runtime.Version()))
}

func displayHelp() {
	helpStr := `
	Command line options
	--------------------
	-help               display help info (this page)

	-samples            display sample requests

	-version            display version number
	
	-newconfig          creates new jsonsqlbrigde_config.json 
	                    configuration file (if none exist) 
	
	-loglevel=[int]     (default=91) -> no item 4/debug)
						 1 errors
						 2 init
						 4 debug
						 8 sql query  
						16 requests   
						32 authentication
						64 performance
						or any combination thereof, example:
						63 gives full logging (1+2+4+8+16+32+64 = 127)
						10 for '2 init' and 8 'sql query' -> as  2 + 8 = 10
	
	-loglevelsql=[int]  (default=0)
						 1 log errors
						 2 log messages
						 4 log rows affected
						 8 trace sql statements
						16 log statement parameters
						32 log transaction begin/end
						or any combination like 3 (equals 1+2) 
						or 63 (1+2+4+8+16+32) log all 
	
	-logstring=[string] displays message on each log entry (default='')
	`
	fmt.Printf(helpStr + "\n")
}

func displaySamples() {
	str := `
	
	Select:
	------  
	http://localhost:3344/sql/v1/select --> HTTP.PUT
	
	(request body)
	{  
		"user"    : "user21",
		"password": "secret",
		"database": "note",
		"query"   : "select n.* from notes n where n.name = ? and n.id >= ?",
		"paras"   : [ "linux", 24 ]
	}
	
	update:
	-------
	http://localhost:3344/sql/v1/update --> HTTP.PUT
	
	(request body)
	{  
		"user"      : "read",
		"password"  : "secret",
		"database"  : "note",
		"table"     : "notes",
		"primarykey": "id",
		"data": {  
				"id"     : 24,
				"body"   : "this is updated body",
				"subject": "Nautilus",
				"updated": "2017-11-14"
		}
	}

	-> Note: you must include the rows primary key, and must add "primarykey":"keyId" (unless your primary key is 'id')

	insert:
	-------
	http://localhost:3344/sql/v1/insert --> HTTP.PUT
	*request body:
	{  
	   "user":"read",
	   "password":"secret",
	   "database":"note",
	   "table":"notes",
	   "data":{  
		  "body":" this a new insert232 - '' 1=1 --",
		  "subject":"Nautilus",
		  "updated":"2017-11-14",
		  "created":"2017-11-14"
	   }
	}
	-> Note: Do pass not the primary key, but if you do, then add "primarykey":"keyId" (unless your primary key is 'id')
	`
	fmt.Printf(str + "\n")
}

func displayVersion() {
	fmt.Printf(f("JSON-SQL Brigde version=%s\n", Version))
}

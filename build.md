##compile distibution 
GOOS=linux   GOARCH=amd64 go build -o json-2-sql_linux     main.go && 
GOOS=darwin  GOARCH=amd64 go build -o json-2-sql_osx       main.go 

##compile distibution (without windows terminal)
### wont work as SQLIte contains CGO 
wont compile: GOOS=windows GOARCH=amd64 go build -o json-2-sql.exe -ldflags -H=windowsgui main.go 


## Use Linux to cross compile to windows compile (required as sqlite incl cgo)
apt-get install gcc-multilib
apt-get install gcc-mingw-w64

### 32 bit
env CGO_ENABLED=1 GOOS=windows GOARCH=386 CC="i686-w64-mingw32-gcc" go build -o gohtmlgui_win32.exe

###64-bit 
env CGO_ENABLED=1 GOOS=windows GOARCH=amd64  CC="x86_64-w64-mingw32-gcc" go build -o gohtmlgui_win64.exe
env CGO_ENABLED=1 GOOS=windows GOARCH=amd64  CC="x86_64-w64-mingw32-gcc" go build -o /media/sf_shared/win64exe/gohtmlgui_win64.exe 


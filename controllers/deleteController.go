package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.com/larsklingsten/gojson2sql/models"
)

// Delete update a row in a database
func (ctrl *Ctrl) Delete(w http.ResponseWriter, r *http.Request) {
	var response models.ServerResponse

	userReq := r.Context().Value(contextRequestKey).(models.UserRequest)

	_, rows, err := ctrl.Svc.SQLDelete(userReq)

	if err != nil {
		response = models.ServerResponse{IsSuccess: false, Message: "Failed to execute update. ", Error: err.Error(), Data: nil}
		fmt.Fprintf(w, response.JSON())
		return
	}

	msg := "Delete Success"
	if rows == 0 {
		msg = "Delete. No rows effected"
	}

	response = models.ServerResponse{IsSuccess: true, Message: msg, Rows: int(rows)}
	fmt.Fprintf(w, response.JSON())
}

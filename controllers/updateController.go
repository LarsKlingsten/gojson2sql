package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.com/larsklingsten/gojson2sql/models"
)

// Update update a row in a database
func (ctrl *Ctrl) Update(w http.ResponseWriter, r *http.Request) {
	var response models.ServerResponse

	userReq := r.Context().Value(contextRequestKey).(models.UserRequest)
	rows, err := ctrl.Svc.SQLUpdate(userReq)

	if err != nil {
		response = models.ServerResponse{IsSuccess: false, Message: "Failed to execute update. ", Error: err.Error()}
		fmt.Fprintf(w, response.JSON())
		return
	}

	msg := "Update Success"
	if rows == 0 {
		msg = "Update. No rows effected"
	}

	response = models.ServerResponse{IsSuccess: true, Message: msg, Rows: int(rows)}
	fmt.Fprintf(w, response.JSON())
}

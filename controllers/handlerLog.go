package controllers

import (
	"net/http"

	sn "bitbucket.com/larsklingsten/gosnippets"
)

var countRequests int

// AddLog give the time to handle the request
func (ctrl *Ctrl) AddLog(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Method != "OPTIONS" { // disregard options (pre-flight stuff)
			countRequests++
			sn.Log2(sn.LogRequest, f("req=%d type=%s url=%s remote=%s ", countRequests, r.Method, r.URL, r.RemoteAddr))
		}

		next.ServeHTTP(w, r)

	})
}

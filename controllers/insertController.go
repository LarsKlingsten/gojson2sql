package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.com/larsklingsten/gojson2sql/models"
)

// Insert inserts a row in a database
func (ctrl *Ctrl) Insert(w http.ResponseWriter, r *http.Request) {
	var response models.ServerResponse

	userReq := r.Context().Value(contextRequestKey).(models.UserRequest)
	id, rows, err := ctrl.Svc.SQLInsert(userReq)

	if err != nil {
		response = models.ServerResponse{IsSuccess: false, Message: "Failed to execute insert. ", Error: err.Error(), Data: nil}
		fmt.Fprintf(w, response.JSON())
		return
	}

	msg := "Insert Success"
	if rows == 0 {
		msg = "Insert. No rows effected"
	}

	response = models.ServerResponse{IsSuccess: true, Message: msg, ID: id, Rows: int(rows)}
	fmt.Fprintf(w, response.JSON())
}

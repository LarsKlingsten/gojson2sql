package controllers

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

// Autenticate checks whether the user is allows on the system
// and also extracts userRequests and save it in context
func (ctrl *Ctrl) Autenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		userReq, err := ctrl.ParseUserRequest(r)

		if err != nil {
			myErr := errors.New(f("Failed to parse request err=%s", err.Error()))
			sn.Log2(sn.LogError, myErr.Error())
			ctrl.failRequest(w, r, myErr)
			return
		}

		if !ctrl.isUserAllowedAccess(userReq) {
			myErr := errors.New(f("Failed to Authenticate user=%s for db=%s", userReq.User, userReq.Database))
			sn.Log2(sn.LogError, myErr.Error())
			ctrl.failRequest(w, r, myErr)
			return
		}

		userReq.AvoidSQLInjection()

		ctx := context.WithValue(r.Context(), contextRequestKey, userReq)

		sn.Log2(sn.LogAuthentication, f("Authenticated user=%s for db=%s", userReq.User, userReq.Database))
		sn.Log2(sn.LogAuthentication, f("TODO // we are not yet checking select, update, delete, insert rights"))

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (ctrl *Ctrl) isUserAllowedAccess(userReq models.UserRequest) bool {

	db, err := ctrl.Svc.FindDatabase(userReq.Database)
	if err != nil {
		sn.Log2(sn.LogAuthentication, f("Database=%s does not exist", userReq.Database))
		return false
	}

	users := db.GetConfig().Users

	for _, u := range users {
		sn.Log2(sn.LogDebug, f("testing user=%s password=%s against reqUser=%s reqPassword=%s",
			u.Name, u.Password, userReq.User, userReq.Password))

		if u.Password == userReq.Password && u.Name == userReq.User {
			return true
		}
	}

	return false
}

func (ctrl *Ctrl) failRequest(w http.ResponseWriter, r *http.Request, err error) {
	r.Header.Add("authenticated", "false")
	r.Header.Add("status", err.Error())
	response := models.ServerResponse{IsSuccess: false, Message: "Request Failed", Error: err.Error()}
	fmt.Fprintf(w, response.JSON())
}

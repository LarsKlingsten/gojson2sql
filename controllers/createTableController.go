package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.com/larsklingsten/gojson2sql/models"
)

// CreateTable creates a new table in the database
func (ctrl *Ctrl) CreateTable(w http.ResponseWriter, r *http.Request) {
	var response models.ServerResponse

	userReq := r.Context().Value(contextRequestKey).(models.UserRequest)
	id, rows, err := ctrl.Svc.SQLCreateTable(userReq)

	if err != nil {
		response = models.ServerResponse{IsSuccess: false, Message: "create table: failed", Error: err.Error(), Data: nil}
		fmt.Fprintf(w, response.JSON())
		return
	}

	msg := "create table: success"
	response = models.ServerResponse{IsSuccess: true, Message: msg, ID: id, Rows: int(rows)}
	fmt.Fprintf(w, response.JSON())
}

// DropTable creates a new table in the database
func (ctrl *Ctrl) DropTable(w http.ResponseWriter, r *http.Request) {
	var response models.ServerResponse

	userReq := r.Context().Value(contextRequestKey).(models.UserRequest)
	id, rows, err := ctrl.Svc.SQLDropTable(userReq)

	if err != nil {
		response = models.ServerResponse{IsSuccess: false, Message: "drop table: failed", Error: err.Error(), Data: nil}
		fmt.Fprintf(w, response.JSON())
		return
	}

	msg := "drop table: success"

	response = models.ServerResponse{IsSuccess: true, Message: msg, ID: id, Rows: int(rows)}
	fmt.Fprintf(w, response.JSON())
}

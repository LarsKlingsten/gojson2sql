package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	"bitbucket.com/larsklingsten/gojson2sql/service"
	sn "bitbucket.com/larsklingsten/gosnippets"
)

type key int

const (
	contextRequestKey key = iota
)

// Ctrl envoironement
type Ctrl struct {
	Svc *service.Service
}

const portStr string = ":3344"
const base string = "/sql"
const version string = "/v1"

var f = fmt.Sprintf

// NewCtrl is constructor for all controllers
func NewCtrl(newsvc *service.Service) {
	logServerDetails()

	ctrl := &Ctrl{Svc: newsvc} // save newSvc to Ctrl struct

	mux := http.NewServeMux()
	mux.HandleFunc(base+version+"/select", ctrl.sqlSelect)
	mux.HandleFunc(base+version+"/update", ctrl.Update)
	mux.HandleFunc(base+version+"/insert", ctrl.Insert)
	mux.HandleFunc(base+version+"/createtable", ctrl.CreateTable)
	mux.HandleFunc(base+version+"/droptable", ctrl.DropTable)
	mux.HandleFunc(base+version+"/delete", ctrl.Delete)
	mux.HandleFunc(base+version+"/version", ctrl.Version)

	err := http.ListenAndServe(portStr, ctrl.AddMeasureTime(ctrl.AddLog(ctrl.Autenticate(ctrl.SetHeaders(mux)))))
	if err != nil {
		sn.LogFatal("@MainController - Fatal Error. Could not start http service. " + err.Error())
	}
}

func logServerDetails() {
	ipAddress := sn.GetLocalIP()
	sn.Log2(sn.LogInitMsg, f("init /controllers/controller.go try http://%s%s%s%s/help", ipAddress, portStr, base, version))
}

func (ctrl *Ctrl) badParameter(parameter string) string {
	return f("Bad request. Was missing ?%s=x", parameter)
}

// ParseUserRequest decodes the incoming request from the request body to UserRequest object
func (ctrl *Ctrl) ParseUserRequest(r *http.Request) (models.UserRequest, error) {

	var userRequest models.UserRequest
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)

	sn.Log2(sn.LogDebug, string(body))

	if err != nil {
		return userRequest, errors.New(f("Failed to read request body err=s%", err.Error()))
	}

	err = json.Unmarshal(body, &userRequest)
	if err != nil {
		return userRequest, errors.New("failed to extract request from Json")
	}

	return userRequest, nil
}

// Version returns the version number os as a Response object
func (ctrl *Ctrl) Version(w http.ResponseWriter, r *http.Request) {

	response := models.ServerResponse{IsSuccess: true, Message: "Version " + service.Version}
	fmt.Fprintf(w, response.JSON())
}

package controllers

import (
	sn "bitbucket.com/larsklingsten/gosnippets"
	"net/http"
	"time"
)

// AddMeasureTime give a performance log entry
func (ctrl *Ctrl) AddMeasureTime(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		start := time.Now()
		next.ServeHTTP(w, r)

		// performance log
		end := time.Now()
		if r.Method != "OPTIONS" {
			sn.Log2(sn.LogPerformance, f("request executed in %d ms", end.Sub(start).Nanoseconds()/1000000.0))
		}
	})
}

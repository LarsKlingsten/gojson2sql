package controllers

import (
	"net/http"
)

// SetHeaders sets the headers for all responses
func (ctrl *Ctrl) SetHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		next.ServeHTTP(w, r)
	})
}

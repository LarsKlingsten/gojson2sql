package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.com/larsklingsten/gojson2sql/models"
)

func (ctrl *Ctrl) sqlSelect(w http.ResponseWriter, r *http.Request) {
	var response models.ServerResponse

	userReq := r.Context().Value(contextRequestKey).(models.UserRequest)

	// dbResult, err := ctrl.Svc.SQLSelect(userReq)
	dbResult, rowsCount, err := ctrl.Svc.SQLSelect(userReq)
	if err != nil {
		response = models.ServerResponse{IsSuccess: false, Message: "Failed to execute query. ", Error: err.Error(), Data: nil}
		fmt.Fprintf(w, response.JSON())
		return
	}

	response = models.ServerResponse{IsSuccess: true, Message: "Query Success", Data: dbResult, Rows: rowsCount}
	fmt.Fprintf(w, response.JSON())
}

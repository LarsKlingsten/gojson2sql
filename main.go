package main

import (
	"fmt"

	"bitbucket.com/larsklingsten/gojson2sql/controllers"
	"bitbucket.com/larsklingsten/gojson2sql/db"
	"bitbucket.com/larsklingsten/gojson2sql/enums"

	"bitbucket.com/larsklingsten/gojson2sql/models"
	"bitbucket.com/larsklingsten/gojson2sql/service"
	sn "bitbucket.com/larsklingsten/gosnippets"

	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/mattn/go-sqlite3"
)

var f = fmt.Sprintf

func main() {
	test()

	// 	sn.Log2(sn.LogInitMsg, AppName+" "+Version)

	service.CommandLineOptions()

	databases, err := CreateSQLServerConnections()
	if err != nil {
		sn.LogFatal("Fatal error. " + err.Error())
		sn.LogFatal("try --help or generated new config file with --newconfig")
		return
	}

	// inject DB in to service
	service, err := service.NewService(databases)
	if err != nil {
		sn.LogFatal("Fatal error." + err.Error())
		return
	}

	service.PrintDatabase() // when debug flag is set
	controllers.NewCtrl(service)
}

// CreateSQLServerConnections return connection strings
func CreateSQLServerConnections() ([]models.IDatabase, error) {

	servers, err := models.LoadDatabasesFromConfig()
	if err != nil {
		return nil, err
	}

	// create and get db instance
	var databases []models.IDatabase
	for _, server := range servers {
		msg := ""
		if !server.IsEnabled {
			sn.Log2(sn.LogInitMsg, f("%s is not enabled", server.Name))
			continue
		}

		switch server.ServerType {

		case enums.MsSQL:
			database, err := db.NewMsSQL(server)
			if err == nil {
				databases = append(databases, database)
			}
			msg = connectionMsg(server, err)

		case enums.SQLite:
			database, err := db.NewSQLite(server)
			if err == nil {
				databases = append(databases, database)
			}
			msg = connectionMsg(server, err)

		default:
			sn.Log2(sn.LogInitMsg, f("%s has unknown server type '%s' (check gosqlconverter.json and use %v)", server.Name, server.ServerType, enums.ServerTypes))
		}

		sn.Log2(sn.LogInitMsg, msg)

	}
	sn.Log2(sn.LogInitMsg, f("Usable Servers=%d", len(databases)))
	return databases, nil
}

func connectionMsg(server models.Server, err error) string {
	if err != nil {
		return f("%s: Connection to %s: failed err=%s", server.ServerType, server.Name, err.Error())
	}
	return f("%s: Connection to %s: success", server.ServerType, server.Name)
}

func test() {

	//	sn.Log(sn.StringRemoveNonAlphaNum("hej med dig111 s12##€ !!"))

	// 	users := []User{
	// 	User{Name: "admin", Password: "secret", Select: true, Insert: true, Update: true, Delete: true},
	// 	User{Name: "read", Password: "secret", Select: true, Insert: false, Update: false, Delete: false},
	// }

	// userRequest := models.UserRequest{Database: "test", User: "lars", Password: "pass", Query: "select * from test", Paras: []string{"123", "13"}}
	// fmt.Printf("%+v\n", userRequest)
	// sn.Log(string(userRequest.JSONAsBytes()))

}

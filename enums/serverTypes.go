package enums

// ServerType is an enum
type ServerType string

//Database types
const (
	MsSQL  ServerType = "MsSQL"
	MySQL  ServerType = "MySQL" //not implemented
	SQLite ServerType = "SQLite"
)

// ServerTypes lists available servers
var ServerTypes = []ServerType{
	MsSQL, MySQL, SQLite,
}

-- to run --
------------
-- sqlite3 sqlite-temperatur.db
-- .read createtables.sql


DROP TABLE IF EXISTS readings;
CREATE TABLE readings  (
  ID INTEGER NOT NULL PRIMARY KEY,
  fk_device INTEGER NOT NULL,
  fk_readingtype INTEGER NOT NULL,
  reading REAL NOT NULL,
  updated TEXT NOT NULL,
   
  FOREIGN KEY (fk_device) REFERENCES device (ID),
  FOREIGN KEY (fk_readingtype) REFERENCES readingtypes (ID)
) ;  

INSERT INTO readings VALUES (2,1,1,12.12,'2000-01-01 00:00:00');

-----------------------------

DROP TABLE IF EXISTS devices;

CREATE TABLE devices  (
  ID INTEGER NOT NULL PRIMARY KEY,
  deviceName TEXT NOT NULL DEFAULT 'not set',
  deviceID TEXT NOT NULL DEFAULT 'not set',
  manufactourerName TEXT NOT NULL DEFAULT 'not set',
  location TEXT  NOT NULL DEFAULT 'not set',
  modelID TEXT NOT NULL DEFAULT 'not set',
  nameOnDevice TEXT NOT NULL DEFAULT 'not set',
  address TEXT NOT NULL DEFAULT 'blichersgade 14' 
) ;


INSERT INTO devices VALUES 
(1,'Preasure 3','00:15:8d:00:01:ab:71:a6','Lumi','bathroom','lumi.weather','1','blichersgade 14'),
(2,'Preasure 6','00:15:8d:00:01:ab:27:8c','Lumi','outside','lumi.weather','6','blichersgade 14'),
(3,'Preasure 9','00:15:8d:00:01:e4:55:41','Lumi','kitchen','lumi.weather','9','blichersgade 14'),
(4,'Preasure 12','00:15:8d:00:01:ab:72:25','Lumi','cinema','lumi.weather','12','blichersgade 12'),
(5,'Preasure 15','00:15:8d:00:01:ab:25:6c','Lumi','gamingroom','lumi.weather','15','blichersgade 12');
 

------------------------------------------------- -

DROP TABLE IF EXISTS readingtypes;
CREATE TABLE readingtypes (
  ID INTEGER NOT NULL PRIMARY KEY,
  readingType  TEXT NOT NULL DEFAULT '(ex temperature)',
  readingTypeShort TEXT  NOT NULL DEFAULT 'not set (ex temp)',
  readingProtocol TEXT NOT NULL DEFAULT 'not set (ex celcius)',
  readingProtocolShort TEXT NOT NULL DEFAULT 'not set (ex C)',
  convertionMultiply REAL NOT NULL DEFAULT '1.00' ,
  rangeMinimum REAL   NOT NULL DEFAULT '-1000000.0',
  rangeMaximum REAL NOT NULL DEFAULT  '100000.0' 
) ;
 
INSERT INTO readingtypes VALUES 
(1,'temperature','temp','celcius','C',0.010000,-50.000000,100.000000),
(2,'humidity','humid','procent','pct',0.010000,0.000000,100.000000),
(3,'pressure','bar','bar','bar',0.010000,500.000000,1500.000000);
 
 select * from readings;
 
 select devices,ID, devices.deviceName, devices.deviceID, readings.id, readings.reading, reading.updated from devices inner join readings on readings.fk_device = devices.ID;



 select devices.ID, devices.deviceName, devices.deviceID, devices.location,
        readings.id, readings.reading, readingtypes.readingTypeShort as type, readings.updated
       
 from devices 
 inner join readings on readings.fk_device = devices.ID
 inner join readingtypes on readings.fk_readingType = readingtypes.ID
 order by devices.ID,  readingtypes.readingTypeShort desc, readings.updated
 ;

-- .read select-all.sql
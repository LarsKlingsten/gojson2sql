#GO JSON-2-SQL 

##Description:
Server that processes JSON objects via HTTP to SQL-Server
-> select, insert, update and delete statements via HTTP

###Versions  
* 0.04.2 | SQLite support, older MS-SQLServer versions support,
* 0.04.1 | create table, drop table, new gojson2sql.json format, XML Dependency removed
* 0.03.4 | delete
* 0.03.3 | insert
* 0.03.1 | update
* 0.03.0 | select statements with parameters

###Dependicies (if you are bulding from source)
```
go get -u github.com/denisenkom/go-mssqldb 
go get -u github.com/mattn/go-sqlite3
go get -u bitbucket.com/larsklingsten/snippets 

```

##How to get started
download binaries to your platform 

#### create configuation file
*   ./jsontosqlXXXX --newconfig   (or jsontosqlWin32.exe --newconfig)

#### update "jsonsqlbridge_config.json"
 changed login/password, type, and database 

## INSERT
### PUT http://localhost:3344/sql/v1/insert
`
{  
   "user":"read",
   "password":"secret",
   "database":"sqlite-test",
   "table":"keyvalue",
   "data":{  
      "KEY": "1252",
      "value" : "this is a test"
   }
}

`
## SELECT ('select' is not be be used in 'query')
### PUT http://localhost:3344/sql/v1/query
`
{  
   "user":"read",
   "password":"secret",
   "database":"sqlite-test",
   "query": "* from keyvalue where key = ?",
   "paras": ["1252"]
 
}


## DELETE 
### PUT http://localhost:3344/sql/v1/delete
`
{  
   "user":"read",
   "password":"secret",
   "database":"sqlite-temperatur",
   "table":"devices",
   "data": {
   "ID": 7
   }
  }
`

## CREATE TABLE
### PUT http://localhost:3344/sql/v1/createtable

`
{  
   "user":"read",
   "password":"secret",
   "database":"sqlite-test",
   "query": "TABLE keyvalue2 (key TEXT NOT NULL PRIMARY KEY, value TEXT NOT NULL);"
}
`

## DROP TABLE
### PUT http://localhost:3344/sql/v1/droptable
`
{  
   "user":"read",
   "password":"secret",
   "database":"sqlite-test",
   "query": "TABLE keyvalue2" 
}
`

## SELECT COMPLEX (again do not use 'select' in query as is auto inserted)
### PUT http://localhost:3344/sql/v1/select

` 
{  
   "user":"read",
   "password":"secret",
   "database":"sqlite-temperatur",
   "table":"keyvalue",
   "query": "devices.ID, devices.deviceName, devices.deviceID, readings.id, readings.reading, readings.updated, readingtypes.readingTypeShort from devices inner join readings on readings.fk_device = devices.ID inner join readingtypes on readings.fk_readingType = readingtypes.ID"
  }
      
` 

### sample usage in dart (which does not have a driver for  MS SQL Server)###
`
import 'package:http/http.dart' as http;

main(List<String> arguments) {
  var url = "http://localhost:3344/sql/v1/select";

  var req = """
    {
      "secret": "mySecret",
      "database": "Vega",
      "query": "select * from vehicles where id = ?",
      "paras": [ 24 ]
    }""";

  http
      .put(url, body: req)
      .then((res) => res.body)
      .then((value) => print(value));
}
`

### Errors
####  Fatal Error. Could not start http service. listen tcp :3344: bind: address already in use 
  * kill -9 $(lsof -ti tcp:3344) && go run main.go